## How to develop the app

**Initialize the environment**

1\. Clone the repository:

```
git clone https://bitbucket.org/KaungKZ/ka_za.git
```

2\. Install the dependencies:

```bash
npm install
```

3\. Build any necessary building/prep tasks

```
npm run build
```

**Launch the app**

1\. Run the development server:

```bash
npm start
```

**live view**


[click here](https://fe-test.netlify.com/)


## Front-End Test for Styling & JS - Read Me Instructions

**Requirements & Instructions:**

**[*]** Do not underestimate this test in terms of implementation and your submission deadline. Each
aspect of the test and instructions test certain specific skills required by our team & are scored.

**[*]** The design for desktop and responsive mobile have been provided for each screen. Please follow it
very closely and replicate them as closely as you can

**[*]** You have the option to use the PSD file (see "PSD File" folder) or also JPG images of each page to
replicate the designed pages (see "JPG Files" folder). In the “Resources” folder you will find the profile
image.

**[*]** Notice that separate designs for desktop and mobile is provided for each screen

**[*]** Tablet design is not provided, but it will be the same as desktop except less wide.

**[*]** The desktop design for field editing popup and hover (for edit pen) elements are shown. The pop-up
design for other fields (such as website, name, etc) are not provided in design (only the pop-up for the city
as an example is shown) but you can use the same concept as shown for those fields in desktop version.
Editing in mobile is shown in the design and is completely different as you can see. Design inconsistencies
between desktop and mobile are intentional.

**[*]** Content for the popups should be generated/saved dynamically. Don’t hardcode content on each
pop-up for example and make it functional. Replace content live as it is edited/saved in both the view
section of the page and the top header (for fields being shown). Show us your scripting skills always in all
pages! We recommend to use ES6 for this.

**[ ]** The menu (about, settings, option1, etc) for mobile version should operate by swapping to the right or
left as you can see in the mobile design. Again, it must be swappable and we should not see a scroll bar
when testing it on both different types of browsers! (see design look)
**_Swipeable nav-bar for mobile version has issues (you can't continue swipping from where you have left, you can swipe even the nav bar ends or before starts)_**

**[*]** The header for the profile page both in desktop and mobile must be a shared component for all
pages/tabs so that if you make a change to the header once, it will change the header automatically for all
other pages/tabs. Changing through the menu tabs should be dynamic, rendering each tab.

**[*]** For icons (such as plus, star, pen, etc), use: http://ionicons.com/cheatsheet.html

**[*]** All fields should follow the Google material design which has the title of the field in the text box by
default; once the user input is provided, the title moves above the field and becomes a small overhead title
and line becomes blue when focused; see for example: https://material.angularjs.org/latest/demo/input ---
however, please note that you should not use angular or other “js frameworks” for this test, only ES6 JS
(which is the only JS used on our platform along with Jquery). Again, the link to material design is for
example purpose only.

**[*]** Must use BEM (http://getbem.com/introduction/) to organize CSS (we use this technology in our
platform)

**[*]** Must use SASS for precompiler (we use this technology in our platform)

**[*]** Must use Gulp for SASS and Webpack for ES6 part (we use these technologies in our platform). Or
just Webpack. We would like to particularly see your understanding of WebPack.

**[*]** Test your pages in the following browsers and version for compatibility: Chrome (older and newest
version), Edge (older and newest version), and Safari (older and newest version) to make sure the pages
look the same as the design and there are no issues with responsive pages. We will view your results in
these 3 browsers and two versions for each (older and newest) on both desktop and mobile sizes. If you
don’t have any of these browsers and can’t download them, signup for a free trial at browserstack.com
(which is what we use in our team). Browser compatibility development is a major frontend skill and points
will be deducted if your work is not compatible.

**[*]** When you start coding this test, in the very beginning, setup Bitbucket repo and commit as you continue
to code and make progress until you’re done & make the final commit. After you’re done and you share the
link for review (only after completion), we will need to see your commit times/dates.

**[*]** Please note that the UI design that is being provided to you is specifically designed for this test and is
not the current or working platform’s design for confidentiality reasons. Please do not worry about the UI/UX
issues and there is no need to make recommendations for UI. Also, note that design inconsistencies are
intentional.

**[*]** Attention to details and following these instructions, AND testing is VERY important aspect of this test.

**[*]** FINALLY, use best programming practices because although we have given you these instructions and
minimum requirements, at the end of the day, when we review your code, we need to note your skills,
standard or expected practices, and your weight with best approaches in frontend programming. Structure
and MVC is very important and part of our scoring system; as a tip, try to avoid hardcoding html into JS (as
possible) and make your JS more component based instead of creating one single file.

---