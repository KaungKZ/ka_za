import "./css/style.scss";

import {
  header_nav,
  edit_btnPhoneSize,
  edit_btns,
  nav_links
} from "./js/selectors.js";

import { handleNavLink } from "./js/navBar.js";
import { handleEdit } from "./js/edit.js";
import { handlePhoneEdit } from "./js/edit.js";
import { touchstarted } from "./js/swipeable.js";
import { touchmoved } from "./js/swipeable.js";
import { touchended } from "./js/swipeable.js";

// event listeners
nav_links.forEach(link => link.addEventListener("click", handleNavLink));
edit_btns.forEach(btn => btn.addEventListener("click", handleEdit));
edit_btnPhoneSize.addEventListener("click", handlePhoneEdit);
header_nav.addEventListener("touchstart", touchstarted);
header_nav.addEventListener("touchmove", touchmoved);
// header_nav.addEventListener("touchend", touchended);
