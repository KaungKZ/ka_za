// variables

import {
  header_nav,
  nav_links,
  main_links,
  edit_btns,
  main_content,
  edit_btnPhoneSize,
  orig_website_name,
  orig_contact_name,
  orig_location_name
} from "./selectors.js";

// functions

import { selectOne } from "./selectors.js";
import { selectAll } from "./selectors.js";
import { htmlForDesktopEdit } from "./htmls.js";
import { htmlForPhoneEdit } from "./htmls.js";
// import { touchstarted } from "./swipeable.js";
// import { touchmoved } from "./swipeable.js";
// import { touchended } from "./swipeable.js";
// import {handleNavLink} from './navBar.js';
// Nav Links clicked

// desktop edit btn clicked
export function handleEdit() {
  const dataValue = this.dataset.refer;

  // get the index to put the selected title on the card
  let index;
  edit_btns.forEach((btn, i) => (this === btn ? (index = i) : ""));

  const isEditingExist = document.querySelector(".main__editing");
  isEditingExist ? isEditingExist.parentNode.removeChild(isEditingExist) : "";

  // calcuate edit card position
  getCoordinates(dataValue);

  let editing_card = document.createElement("div");

  htmlForDesktopEdit(editing_card, index);
  handleLiveEdit(dataValue);

  editing_card.style.left = `${nameX + 10}px`;
  editing_card.style.top = `${nameY - 10}px`;

  // event listeners for edit and save btns

  selectOne(".main__editing-save").addEventListener(
    "click",
    handleEditSaveAndCancel
  );
  selectOne(".main__editing-cancel").addEventListener(
    "click",
    handleEditSaveAndCancel
  );
}

// calculate which input they've selected and display respective name
export function handleLiveEdit(dataValue) {
  const className = dataValue.split("__")[1];
  let selectedElements = [];

  if (className === "website") {
    selectedElements.push(orig_website_name);
  } else {
    selectAll(`.${className}`).forEach(name => selectedElements.push(name));
  }

  let edit_input = selectOne(".main__editing__input");

  edit_input.value = selectedElements[0].innerText.trim();

  // event listener for live inpput change

  edit_input.addEventListener("input", () =>
    handleInputChange(selectedElements, edit_input.value)
  );
}

// live input changed

export function handleInputChange(selectedElements, inputValue) {
  return selectedElements.map(name => (name.innerHTML = inputValue));
}

//  save and cancel features for desktop

function handleEditSaveAndCancel() {
  const isEditingExist = document.querySelector(".main__editing");
  isEditingExist ? isEditingExist.parentNode.removeChild(isEditingExist) : "";
}

// get coordinates of the clicked element
let nameX, nameY;
function getCoordinates(dataValue) {
  const clickedElement = selectOne(`.${dataValue}`);
  nameX = clickedElement.getBoundingClientRect().width;
  nameY = clickedElement.offsetTop;
}

// -------------- desktop editing finished --------------------

// phone editing
export function handlePhoneEdit() {
  const isPhoneExisting = selectOne(".main__phone-editing");
  isPhoneExisting
    ? isPhoneExisting.parentNode.removeChild(isPhoneExisting)
    : "";

  const phone_editCard = document.createElement("div");

  // generate html for phone editing function
  htmlForPhoneEdit(phone_editCard);

  main_content.appendChild(phone_editCard);

  const phone_editInput = selectAll(".main .main__phone-input");

  // event listener for live input change
  phone_editInput.forEach(input =>
    input.addEventListener("input", e => handlePhoneInputChange(e))
  );

  // event listeners for save and cancel features

  selectOne(".main__phone-save-btn").addEventListener("click", () =>
    handlePhoneSaveAndCancel()
  );
  selectOne(".main__phone-cancel-btn").addEventListener("click", () =>
    handlePhoneSaveAndCancel()
  );
}

// calculate which input user types and edit live in html

export function handlePhoneInputChange(e) {
  const elementIDName = e.target.id.toLowerCase();
  let changedValue = e.target.value;
  const firstName = selectAll(".main__name-fn");
  const lastName = selectAll(".main__name-ln");

  elementIDName.includes("first name")
    ? firstName.forEach(name => (name.innerHTML = changedValue))
    : elementIDName.includes("last name")
    ? lastName.forEach(name => (name.innerHTML = changedValue))
    : elementIDName.includes("website")
    ? (orig_website_name.innerHTML = changedValue)
    : elementIDName.includes("phone number")
    ? orig_contact_name.forEach(name => (name.innerHTML = changedValue))
    : orig_location_name.forEach(name => (name.innerHTML = changedValue));
}

// save and cancel features for phone
function handlePhoneSaveAndCancel() {
  const mainEditingCard = selectOne(".main__phone-editing");

  mainEditingCard.parentNode.removeChild(mainEditingCard);
}
