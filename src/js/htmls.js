import {
  main_editContent,
  orig_person_name,
  orig_website_name,
  orig_contact_name,
  orig_location_name
} from "./selectors.js";

export function htmlForDesktopEdit(editing_card, index) {
  let arrTitles = ["Name", "Website", "Phone Number", "City, State & Zip"];

  editing_card.className = "main__editing main__editing--active";
  editing_card.innerHTML = `
      <div class="main__editing__wrapper-input">
        <input
          type="text"
          class="main__editing__input"
          id="main__desktop-edit-input"
          required
          autocomplete="off"
        />
        <label for="main__desktop-edit-input" class="main__editing-label"
          ><span class="main__editing-span"
            >${arrTitles[index]}</span
          ></label
        >
      </div>
      <div class="main__editing-buttons">
        <button class="main__editing-save">Save</button>
        <button class="main__editing-cancel">Cancel</button>
      </div>
   `;

  main_editContent.appendChild(editing_card);
}

// generate html for phone editing
export function htmlForPhoneEdit(phone_editCard) {
  let arrTitles = [
    "First name",
    "Last Name",
    "Website",
    "Phone Number",
    "City, State & Zip"
  ];
  let arrNames = [];
  const firstName = orig_person_name[0].innerText.split(" ")[0];
  const lastName = orig_person_name[0].innerText.split(" ")[1];

  arrNames.push(
    firstName,
    lastName,
    orig_website_name.innerHTML,
    orig_contact_name[0].innerHTML.trim(),
    orig_location_name[0].innerHTML.trim()
  );

  phone_editCard.className = "main__phone-editing";
  phone_editCard.innerHTML = `
    <div class="main__phone-container">
      <div class="main__phone-header">
        <h2 class="main__phone-h main__header">About</h2>
        <div class="main__phone-btns">
          <button class="main__phone-save-btn">Save</button>
          <button class="main__phone-cancel-btn">Cancel</button>
        </div>
      </div>
      <div class="main__phone">
       ${Array(5)
         .fill()
         .map((one, i) => htmlLooping(arrNames[i], arrTitles[i]))
         .join("")}
      </div>
    </div>`;
}
// html template to produce looping index values
export function htmlLooping(val, title) {
  return `<div class="main__phone-fn main__phone-info">
      <input
        type="text"
        class="main__phone-input"
        id="main__phone-edit-input ${title}"
        value="${val}"
        required
        autocomplete="off"
      />
      <label for="main__phone-edit-input ${val}" class="main__phone-label"
        ><span>${title}</span></label
      >
    </div>`;
}
