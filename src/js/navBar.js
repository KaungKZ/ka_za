import { nav_links, main_links } from "./selectors.js";

export function handleNavLink() {
  let linkIndex;

  nav_links.forEach((link, index) => {
    link.classList.remove("header__link--active");
    this === link ? (linkIndex = index) : "";
  });

  this.classList.toggle("header__link--active");
  main_links.forEach(link => link.classList.remove("main__edit--active"));
  main_links[linkIndex].classList.add("main__edit--active");
}
