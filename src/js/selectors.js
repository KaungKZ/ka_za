export const header_nav = selectOne(".header__nav");
export const nav_links = selectAll(".header__link");
export const main_links = selectAll(".main-links");
export const edit_btns = selectAll(".main__edit-btn");
export const main_editContent = selectOne(".main__edit");
export const main_content = selectOne(".main");
export const edit_btnPhoneSize = selectOne(".main__edit-btn--phone-size");
export const orig_person_name = selectAll(".name");
export const orig_website_name = selectOne(".website");
export const orig_contact_name = selectAll(".contact");
export const orig_location_name = selectAll(".location");

export function selectOne(val) {
  return document.querySelector(val);
}

export function selectAll(val) {
  return document.querySelectorAll(val);
}
