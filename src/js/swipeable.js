import { header_nav } from "./selectors.js";

let startingPosition;
export function touchstarted(e) {
  startingPosition = e.touches[0].clientX;
}

export function touchmoved(e) {
  const move = e.touches[0].clientX;

  if (header_nav.getBoundingClientRect().right > 1000) {
    header_nav.style.left = `0`;
  } else {
    const distance = move - startingPosition;
    header_nav.style.left = `${distance}px`;
  }
}
